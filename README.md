# gitee-stargazers

将 Gitee 项目的 Star 数显示到统计图上。

## 开发背景

目前开发者最常用的查看 star 趋势的工具一般都是 [starchart.cc](https://starchart.cc/)，比如：

[![Stargazers over time](https://starchart.cc/justauth/JustAuth.svg)](https://starchart.cc/justauth/JustAuth)

但是这个工具不支持 Gitee 中的仓库，那怎么行！必须安排上！

## 使用方式

### Npm

```console
npm run start
```

### Docker

```bash
# 拉取镜像
docker pull justauth/gitee-stargazers

# 运行容器
docker run -d -p 3000:3000 --name gitee-stargazers justauth/gitee-stargazers
```


### Dockerfile

```bash
# 通过 Dockerfile 构建镜像
docker build -t <your username>/gitee-stargazers .

# 运行容器
docker run -d -p 3000:3000 --name gitee-stargazers <your username>/gitee-stargazers
```

### 访问

浏览器访问：http://localhost:3000/{owner}/{repo}/{repoCreatedTime} .

- owner: 项目所有者，支持组织名、用户名、企业名
- repo：仓库名
- repoCreatedTime：仓库创建时间

例如：[http://localhost:3000/fujieid/jap/2021-1-1](http://localhost:3000/fujieid/jap/2021-1-1)

示例：

[![](docs/img/f99c085c.png)](https://whnb.wang/yadong.zhang/JustAuth/2019-01-31)

## 特别说明

由于受 Gitee 接口限制，目前此项目暂时只能获取到两个月内的 star 数据，获取更多数据的方式，正在和 Gitee 技术沟通。

## 参考资料

`index.html` 和 `style.css` 文件参考并改造自 [caarlos0/starcharts](https://github.com/caarlos0/starcharts)项目
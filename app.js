var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var repoRouter = require('./routes/repo');
var repoChartRouter = require('./routes/repoChart');

var app = express();

app.use(logger('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));



// view engine setup
app.set('views', path.join(__dirname, 'views'));
//设置模板的后缀是html
app.engine('html', require('ejs').renderFile);
// 模板引擎
app.set('view engine', 'html');
app.use(express.static(path.join(__dirname, 'public')));


app.use('/', indexRouter);
app.use('/', repoRouter);
app.use('/img', repoChartRouter);

module.exports = app;

const express = require('express');
const router = express.Router();
const request = require('request')
const fs = require('fs');
const exporting = require("node-highcharts-exporting");
const Utils = require("../util/Utils");

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {
        title: 'JAI - Passport Login Quickstart'
    });
});

module.exports = router;

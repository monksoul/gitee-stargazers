const express = require('express');
const router = express.Router();
const request = require('request')
const fs = require('fs');
const exporting = require("node-highcharts-exporting");
const Utils = require("../util/Utils");

router.get('/:owner/:repo/:startTime', function (req, res, next) {
    renderImg(req, res)
});

function renderImg (req, res) {
    const owner = req.params.owner
    const repo = req.params.repo
    const startTime = req.params.startTime
    request(Utils.parseUrl(owner, repo, startTime), function (err, response, body) {
        let xAxisData = [];
        let seriesData = [];
        if (!err && response.statusCode == 200) {
            const starInfo = JSON.parse(body);
            if (starInfo.status == 200) {
                const starData = starInfo.data;
                let starItem = []
                let starTotal = 0;
                for (let i = 0, len = starData.length; i < len; i++) {
                    starItem = starData[i];
                    const date = starItem[0];
                    const num = starItem[1];
                    xAxisData.push(date);
                    const currentStarTotal = starTotal + num;
                    seriesData.push(currentStarTotal);

                    starTotal = currentStarTotal;
                }
            }
        }

        const xStep = parseInt((seriesData.length / 5).toFixed(0))
        const yStep = parseInt((seriesData.length / 5).toFixed(0))
        exporting({
            width: 900,
            data: {
                chart: {
                    height: 200
                },
                title: {
                    text: null
                },
                legend: {
                    enabled: false
                },
                credits: {
                    enabled:false
                },
                xAxis: {
                    categories: xAxisData,
                    tickInterval: xStep,
                    tickmarkPlacement: 'on',
                    labels: {
                        style: {
                            color: 'gray',
                            fontSize: '8px'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: 'Gitee Repo Stargazers'
                    },
                    gridLineDashStyle: 'longdash',
                    // lineWidth: 1,
                    gridLineWidth: 0.5,
                    min: 0,
                    opposite: true,
                    tickInterval: yStep,
                    tickmarkPlacement: 'on',
                    labels: {
                        style: {
                            color: 'gray',
                            fontSize: '8px'
                        },
                        align: 'left',
                    }
                },
                series: [{
                    type: "line",
                    lineWidth: 1,
                    marker: {
                        enabled: false
                    },
                    data: seriesData
                }]
            },
        }, function (err, data) {
            Utils.writeImgToResponse(data, res);
        })
    })
}

module.exports = router;

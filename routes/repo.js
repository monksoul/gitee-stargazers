const express = require('express');
const router = express.Router();
const request = require('request')
const Utils = require("../util/Utils");

router.get('/:owner/:repo/:startTime', function (req, res, next) {
    aaas(req, res);
});

function aaas(req, res) {
    const owner = req.params.owner
    const repo = req.params.repo
    var startTime = req.params.startTime
    request(Utils.parseUrl(owner, repo, startTime), function (err, response, body) {
        if (!err && response.statusCode == 200) {
            const starInfo = JSON.parse(body);
            let starTotal = 0;
            if (starInfo.status == 200) {
                const starData = starInfo.data;
                let starItem = []
                !startTime ? (startTime = starData[0][0]) : startTime
                for (let i = 0, len = starData.length; i < len; i++) {
                    starItem = starData[i];
                    const num = starItem[1];
                    starTotal = starTotal + num;
                }
            }
            res.render('index', {
                owner: owner,
                repo: repo,
                startTime: startTime,
                starTotal: starTotal,
            });
        }
    })
}

module.exports = router;
